<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Абрамов Владислав 27/1 </title>
    <style>
        /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром.*/
.error {
  border: 2px solid red;
}
    </style>
  </head>
  
  <body>
      <header>
    <h1>Форма</h1>
  </header>
  <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
  <div class="my-blocks">
      <div id="vhod">
          <?php 
          if (empty($_SESSION['login'])){
          ?>
          <a id="gl" href="login.php" >Вход</a>
          
          <?php 
          }else { ?><a id="gl" href="login.php" >Выход</a><?php } ?>
          
        </div>
    <div id="form">
    <form action="" method="POST">
      <label>
        Имя:<br />
        <input type="text"  name="fio"<?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" 
        placeholder="Владислав" />
      </label><br />

      <label>
        Email:<br />
        <input type = "email" name="mail" <?php if ($errors['mail']) {print 'class="error"';} ?> value = "<?php print $values['mail']; ?>"
        placeholder="test@example.com"
          type="email" />
      </label><br />
    
      <label>
          <p>Год рождения:</p>
          <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value = "<?php print $values['year']; ?>">
              <option value="1990" <?php if($values['year'] == 1990) {print 'selected';}?>>1990</option>
              <option value="1991" <?php if($values['year'] == 1991) {print 'selected';}?>>1991</option>
              <option value="1992" <?php if($values['year'] == 1992) {print 'selected';}?>>1992</option>
              <option value="1993" <?php if($values['year'] == 1993) {print 'selected';}?>>1993</option>
              <option value="1994" <?php if($values['year'] == 1994) {print 'selected';}?>>1994</option>
              <option value="1995" <?php if($values['year'] == 1995) {print 'selected';}?>>1995</option>
              <option value="1996" <?php if($values['year'] == 1996) {print 'selected';}?>>1996</option>
              <option value="1997" <?php if($values['year'] == 1997) {print 'selected';}?>>1997</option>
              <option value="1998" <?php if($values['year'] == 1998) {print 'selected';}?>>1998</option>
              <option value="1999" <?php if($values['year'] == 1999) {print 'selected';}?>>1999</option>
              <option value="2000" <?php if($values['year'] == 2000) {print 'selected';}?>>2000</option>
              <option value="2001" <?php if($values['year'] == 2001) {print 'selected';}?>>2001</option>
              <option value="2002" <?php if($values['year'] == 2002) {print 'selected';}?>>2002</option>
              <option value="2003" <?php if($values['year'] == 2003) {print 'selected';}?>>2003</option>
              <option value="2004" <?php if($values['year'] == 2004) {print 'selected';}?>>2004</option>
              <option value="2005" <?php if($values['year'] == 2005) {print 'selected';}?>>2005</option>
              <option value="2006" <?php if($values['year'] == 2006) {print 'selected';}?>>2006</option>
              <option value="2007" <?php if($values['year'] == 2007) {print 'selected';}?>>2007</option>
              <option value="2008" <?php if($values['year'] == 2008) {print 'selected';}?>>2008</option>
              <option value="2009" <?php if($values['year'] == 2009) {print 'selected';}?>>2009</option>
              <option value="2010" <?php if($values['year'] == 2010) {print 'selected';}?>>2010</option>
            </select>
      </label><br />
    
      <label>      
        Пол:<br />
        <input type="radio" name="sex" value="M" <?php if ($values['sex'] == 'M') {print 'checked';} ?> <?php if ($errors['sex']) {print 'class="error"';} ?>/> Мужской
      </label>
      <label>
        <input type="radio" name="sex" value="W" <?php if ($values['sex'] == 'W') {print 'checked';} ?> <?php if ($errors['sex']) {print 'class="error"';} ?>/> Женский
      </label><br />

      <label>    
        Кол-во конечностей:<br />
        <input type="radio" 
          name="countlimbs" value="4" <?php if($values['countlimbs'] == 4) {print 'checked';}?> <?php if ($errors['countlimbs']) {print 'class="error"';} ?> />
          4
      </label>
      <label><input type="radio" 
          name="countlimbs" value="5" <?php if($values['countlimbs'] == 5) {print 'checked';}?> <?php if ($errors['countlimbs']) {print 'class="error"';} ?> />
          5
      </label>
      <label><input type="radio" 
          name="countlimbs" value="2" <?php if($values['countlimbs'] == 2) {print 'checked';}?> <?php if ($errors['countlimbs']) {print 'class="error"';} ?> />
          2
      </label>
      <label><input type="radio"
          name="countlimbs" value="16" <?php if($values['countlimbs'] == 16) {print 'checked';}?> <?php if ($errors['countlimbs']) {print 'class="error"';} ?> />
          16
      </label><br />
    
      <label>
          Какие у вас есть сверхспособности:
          <br />
          <select name="super[]" multiple=multiple>
              <option value="1" <?php if ($errors['super']) {print 'class="error"';} ?> 
              <?php
              $arr = str_split($values['super']);
              foreach($arr as $el)
                if ($el == 1)
                  print 'selected';
            ?>>
            Чтение мыслей
            </option>
              <option value="2" <?php if ($errors['super']) {print 'class="error"';} ?> 
              <?php
              $arr = str_split($values['super']);
              foreach($arr as $el)
                if ($el == 2)
                  print 'selected';
            ?>>
              Предсказывание погоды
              </option>
              <option value="3" <?php if ($errors['super']) {print 'class="error"';} ?> 
              <?php
              $arr = str_split($values['super']);
              foreach($arr as $el)
                if ($el == 3)
                  print 'selected';
            ?>>
              Прохождение сквозь стену</option>
              <option value="4" <?php if ($errors['super']) {print 'class="error"';} ?> 
              <?php
              $arr = str_split($values['super']);
              foreach($arr as $el)
                if ($el == 4)
                  print 'selected';
            ?>>
              Бессмертие</option>
              <option value="5" <?php if ($errors['super']) {print 'class="error"';} ?> 
              <?php
              $arr = str_split($values['super']);
              foreach($arr as $el)
                if ($el == 5)
                  print 'selected';
            ?>>
              Левитация</option>
          </select>
      </label><br />
    
      <label>
          Биография:<br />
          <textarea placeholder="Информация о вас" name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?></textarea>
      </label><br />
    
        Соглашение:<br />
      <label>
      <input type="checkbox" name="check1">
      С контрактом ознакомлен
      </label><br />
    
      <input type="submit" value="Отправить">
    </form>
  </div>
</div>
</body>
</html>
